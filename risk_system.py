import numpy as np
import pandas as pd
from trading_system import record_trade_in_portfolio


class RiskSystem:
    """Interface to buy and sell positions in portfolio."""
    def __init__(self, historic_data, growth_rate_year=.02, return_vola_year=.1, var_perc=.05, var_days=365,
                 var_limit=10_000, num_sims=100):
        self.risk_free_rate_year = growth_rate_year
        self.var_perc = var_perc
        self.var_days = var_days
        self.var_limit = var_limit
        self.num_sims = num_sims
        self.vola_year = return_vola_year
        self.historic_data = historic_data

    @staticmethod
    def simulate_trade(ticker, price, volume, buy, portfolio: {}):
        """Returns copy of portfolio containing new desired trade."""
        # kopie erzeugen
        pf_new = portfolio.copy()
        buy_factor = -1
        if buy:
            buy_factor = 1
        # prüfen ob aktie vorhanden ist
        pf_new[ticker] = portfolio.get(ticker,0) + buy_factor * volume  
        pf_new["cash"] -= buy_factor * volume * price  
        return pf_new



    def is_trade_within_limit(self, ticker, price, volume, buy, portfolio: {}) -> bool:
        """Will the trade be within var limit for portfolio?"""

        pf_new = self.simulate_trade(ticker, price, volume, buy, portfolio)
        var = self.simulate_var_for_portfolio(portfolio=pf_new)

        if self.var_limit - var < 0:
            return False
        else:
            return True

    def _calculate_var(self, future_losses):
        q = - np.quantile((future_losses), self.var_perc)
        if q < 0:
            q = 0
        return q

    def _simulate_portfolio_returns_paths(self, portfolio: {}):
        shape = (len(portfolio)-1,self.var_days,self.num_sims)
        return np.random.normal(self.risk_free_rate_year/self.var_days, self.vola_year/np.sqrt(self.var_days),shape)
         


    def _calculate_future_asset_values(self, sim_values, portfolio: {}):
        dim_reduction = np.multiply.reduce(sim_values + 1, 1)
        pf_values_without_cash = self._get_pf_values_without_cash(portfolio)
        return np.transpose(dim_reduction).dot(np.array(pf_values_without_cash))


    def _get_pf_values_without_cash(self, portfolio: {}):
       # pf_new = portfolio.copy()
       # list_values = []
       # if "cash" in pf_new:
       #     del pf_new["cash"]
       # for ticker, volume in portfolio.items():
       #     price = HistoricData.get_last_price(ticker) * volumne
       #     list_values.append(price)
        return [self.historic_data.get_last_price(ticker) * volume for ticker, volume in portfolio.items() if ticker != "cash"]

    def _get_pf_total_value_without_cash(self, portfolio: {}) -> float:
        values = self._get_pf_values_without_cash(portfolio)
        return sum(values)

    def simulate_var_for_portfolio(self, portfolio: {}) -> float:
        """Returns the value at risk for the current portfolio using monte carlo simulation.
        Uses the settings for percentage, etc. of its RiskSystem instance."""
        sim_values=self._simulate_portfolio_returns_paths(portfolio)
        future_losses=self._get_pf_total_value_without_cash(portfolio)-self._calculate_future_asset_values(sim_values, portfolio)
        return self._calculate_var(future_losses)
