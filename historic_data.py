import sqlite3
import pandas as pd


class HistoricData:
    """AP to historic market data."""
    db_path = r'.\market.db'

    @staticmethod
    def get_last_price(ticker: str) -> float:
        """Returns last available close price for ticker."""
        ticker = ticker.upper()
        sql_str = f"SELECT close FROM prices WHERE symbol = '{ticker}' ORDER BY date DESC LIMIT 1"
        with sqlite3.connect(HistoricData.db_path) as con:
            close = pd.read_sql(sql_str, con=con)
        return float(close.values[0])
