import copy
import pandas as pd


def record_trade_in_portfolio(ticker, price, volume, buy, portfolio):
    """changes portfolio dict in place to record trade. Adjusts position of cash and ticker traded."""
    
    pass


class TradingSystem:
    """Interface to buy and sell positions in portfolio."""
    def __init__(self, portfolio: {}, market_api, risk_api=None):
        self.portfolio = copy.deepcopy(portfolio)
        self.market_api = market_api
        self.risk_api = risk_api

    def get_portfolio_position(self, ticker):
        # TODO: Function needed?
        return self.portfolio.get(ticker, 0)
        

    def trade(self, ticker, volume, buy=True) -> (bool, float):
        """Attempts to buy or sell, returns tuple with success flag and explanatory message in case of failure."""
        market_book = self.market_api.get_market_book(ticker)
        sum_volumn = 0
        if buy:
            sum_volumn = market_book[market_book.side == 'ASK']['volume'].sum()
        else:
            sum_volumn = market_book[market_book.side == 'BID']['volume'].sum()
        if sum_volumn < volume:
            return False, "Not enough market volume!"

        price =  self.calculate_vwap(market_book, volume, buy)
        if self.risk_api.is_trade_within_limit(ticker, price, volume, buy,self.portfolio):
            return True, "OK"
        else:
            return False, "Trade would break limit!"

    @staticmethod
    def calculate_vwap(book: pd.DataFrame, volume: float, buy: bool = True) -> float:
        """Calculate volume weighted average price (vwap) given a market book holding positions.
        Returns null if book holds insufficient volume to calculate vwap for desired volume."""

        if buy:
            book_used = book[book['side']=='ASK']
            book_used.sort_values(by=['price'],ascending=True, inplace=True)
        else:
            book_used = book[book['side']=='BID']
            book_used.sort_values(by=['price'],ascending=False, inplace=True)

        total_price = 0
        volume_remaining = volume

        for i,row in book_used.iterrows():
            if row['volume'] > volume_remaining:
                effective = volume_remaining
            else:
                effective = row['volume']
            total_price += effective * row['price']
            volume_remaining -= effective

        vwap = total_price / volume
        
        if volume_remaining > 0:
            return None
        else:
            return vwap




    def calculate_market_vwap(self, ticker, volume, buy=False) -> float:
        """Calculate volume weighted average price (vwap) for ticker."""
        book = self.market_api.get_market_book(ticker)
        return calculate_vwap(book, volume, buy)
