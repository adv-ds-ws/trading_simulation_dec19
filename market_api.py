import random
import numpy as np
import pandas as pd
import datetime


class MarketAPI:
    """Interface to communicate with stock exchange."""
    # TODO: Implement buffer
    def __init__(self, historic_data=None):
        self.stable = False
        self.last_timeout = datetime.datetime.now()
        self.historic_data = historic_data

    def _get_last_price(self, ticker):
        if self.historic_data is None:
            return ord(ticker[0]) + random.gauss(0, ord(ticker[0]) / 5.)
        else:
            return self.historic_data.get_last_price(ticker)

    def get_market_book(self, ticker: str) -> pd.DataFrame:
        """Returns order book data fro ticker containing ordered bid and ask prices with respective volume"""
        if not ticker.isalnum():
            raise ValueError(f"Ticker '{ticker}' invalid! Must be alphanumeric.")
        return self._calculate_book(ticker)

    def _calculate_book(self, ticker):
        np.random.seed(datetime.datetime.now().second)
        mid_price = self._get_last_price(ticker)
        bid_depth, ask_depth = np.random.randint(3, 9), np.random.randint(3, 9)
        quotes = ["BID"] * bid_depth + ["ASK"] * ask_depth
        price_devs_bid = np.cumsum(np.random.rand(bid_depth) / 100 + .01)[::-1] * -1
        price_devs_ask = np.cumsum(np.random.rand(ask_depth) / 100 + .01)
        prices = np.concatenate((price_devs_bid, price_devs_ask)) + mid_price
        volumes = np.random.randint(10, 100, bid_depth + ask_depth)
        book = pd.DataFrame({"side": quotes, "price": np.round(prices, 3), "volume": volumes},
                            index=list(range(len(prices))))
        book.index.name = "id"
        return book

    def execute_order(self, ticker, volume):
        now = datetime.datetime.now()
        seconds_from_last_timeout = (now - self.last_timeout).seconds
        rand_float = np.random.rand()
        if self.stable or seconds_from_last_timeout < 1 or rand_float < .75:
            status = True
            message = "OK"
        else:
            status = False
            message = "Execution failed, connection timed out!"
            MarketAPI.last_timeout = now
        return status, message
