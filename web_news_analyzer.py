import requests
import re
import pandas as pd



def load_headline_text_from_www(page_num=1):
    result = requests.get(f"https://www.nasdaq.com/news/market-headlines.aspx?page={page_num}")
    return result.content.decode("utf-8")


def load_stock_sentiment_from_www(stock_ticker):
    # url = f"https://www.thestreet.com/quote/{stock_ticker}.html"
    url = f"https://www.barchart.com/stocks/quotes/{stock_ticker}"
    # r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83)'})
    r = requests.get(url, headers={'User-Agent': 'Custom'})
    txt = r.content.decode("utf-8")
    return txt


def return_words_in_text(text, watchlist, max_hits=3):
    words = re.split(r"\W+", text)
    unique_words = set(words)
    stocks_in_the_headlines = []
    for word in unique_words:
        if word.lower() in watchlist:
            stocks_in_the_headlines.append(word.lower())
        if len(stocks_in_the_headlines) == max_hits:
            break
    return stocks_in_the_headlines


def search_headline_pages_for_stock_mentions(watchlist, max_hits=3, max_pages_to_search=3):
    found_in_headlines = []
    for page_num in range(max_pages_to_search):
        text = load_headline_text_from_www(page_num)
        found_on_page = return_words_in_text(text, watchlist, max_hits-len(found_in_headlines))
        found_in_headlines.extend(found_on_page)
        if len(found_in_headlines) == max_hits:
            break
    return found_in_headlines


def get_tip_for_stock(stock_ticker: str) -> str:
    txt = load_stock_sentiment_from_www(stock_ticker)
    if '''class="buy-color"''' in txt:
        rating = "buy"
    elif '''class="hold-color"''' in txt:
        rating = "hold"
    elif '''class="sell-color"''' in txt:
        rating = "sell"
    else:
        rating = "Dont know"
    return rating


def get_stock_tips(max_hits, max_pages_to_search=3) -> (str, str):
    tips = []
    stock_watchlist = load_stock_watchlist()
    stock_mentions = search_headline_pages_for_stock_mentions(stock_watchlist)
    for stock_mention in stock_mentions:
        tip = get_tip_for_stock(stock_mention)
        if tip != "Dont know":
            tips.append((stock_mention, tip))
    return tips


def load_stock_watchlist(path="stock_watchlist.csv"):
    stock_watchlist = pd.read_csv(path)
    return [w.lower() for w in stock_watchlist["stocks"]]
